mkdocs==1.4.2
mkdocs-git-revision-date-localized-plugin==1.2.0
mkdocs-material==9.1.6
mkdocs-material-extensions==1.1.1
mkdocs-with-pdf==0.9.3